// https://expressjs.com/en/advanced/best-practice-performance.html#handle-exceptions-properlys
const asyncWrap =
  (fn) =>
  (...args) =>
    fn(...args).catch(args[2]);

module.exports = asyncWrap;
