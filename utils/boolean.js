const FLAGS_TRUE = ["On", "on", "Yes", "yes", "True", "true"];
const FLAGS_FALSE = ["Off", "off", "No", "no", "False", "false"];
const FLAGS_BOOLEAN = FLAGS_TRUE.concat(FLAGS_FALSE);

function toBoolean(value) {
  switch (value) {
    case "On":
    case "on":
    case "Yes":
    case "yes":
    case "True":
    case "true":
    case "1":
    case 1:
    case true:
      return true;
    case "Off":
    case "off":
    case "No":
    case "no":
    case "False":
    case "false":
    case "0":
    case 0:
    case false:
      return false;
    default:
      throw new Error("Value cannot be converted to boolean");
  }
}

module.exports = { toBoolean, FLAGS_BOOLEAN };
