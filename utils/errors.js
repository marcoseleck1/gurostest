const { config } = require("../config");

const Errors = {
  UNKNOWN: [0, "UNKNOWN"],
  // error code 1 is reserved for VALIDATION_ERROR_CODE in config/settings.js, i.e. HTTP 422 validation errors
  // note that this is a getter as its values can be overwritten at runtime (async AWS Secrets Manager)
  get VALIDATION() {
    return [config.VALIDATION_ERROR_CODE, config.VALIDATION_RESPONSE_MESSAGE];
  },

  build: function (error, entries) {
    const number = String(error[0]).padStart(4, "0");
    const result = {
      code: `${config.SERVICE_CODE}${number}`,
      message: error[1],
    };

    if (entries) {
      result.errors = entries;
    }

    return result;
  },
};

class ValidationError extends Error {
  constructor(errors) {
    super();
    this.name = "ValidationError";
    this.code = Errors.VALIDATION[0];
    this.message = Errors.VALIDATION[1];
    this.errors = errors;
  }

  serializable() {
    return Errors.build(Errors.VALIDATION, this.errors);
  }
}

module.exports = { Errors, ValidationError };
