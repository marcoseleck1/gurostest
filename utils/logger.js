const pino = require("pino");

const options = {
  // this default level may be overridden elsewhere, e.g. src/api.js
  level: "debug",
  formatters: {
    level: (label) => ({ level: label }),
  },
  base: undefined,
  timestamp: pino.stdTimeFunctions.isoTime,
  messageKey: "message",
  nestedKey: "payload",
  serializers: {
    payload: (data) => {
      if (data instanceof Error) {
        return pino.stdSerializers.err(data);
      }
      if (data.req) {
        return {
          method: data.req.method,
          url: data.req.url,
          params: data.req.params,
          query: data.req.query,
          headers: data.req.headers,
          body: data.req.raw.body,
        };
      }
      if (data.res) {
        return {
          statusCode: data.res.statusCode,
          statusMessage: data.res.raw.statusMessage,
          headers: data.res.headers,
          // no easy way to obtain the response body, would need to be set manually by the consumer
        };
      }
      return data;
    },
  },
};

const logger = pino(options);

module.exports = logger;
