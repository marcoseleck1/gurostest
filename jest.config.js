/*
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

module.exports = {
  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // Automatically reset mock state before every test
  resetMocks: true,

  // Automatically restore mock state before every test
  restoreMocks: true,

  // Indicates whether the coverage information should be collected while executing the test
  collectCoverage: true,

  // An array of glob patterns indicating a set of files for which coverage information should be collected
  collectCoverageFrom: ["./src/**/*.js"],

  // A list of reporter names that Jest uses when writing coverage reports
  coverageReporters: ["text"],

  // This will be used to configure minimum threshold enforcement for coverage results
  coverageThreshold: {
    global: {
      statements: 100,
      branches: 90,
      functions: 100,
      lines: 100,
    },
  },

  // A list of paths to modules that run some code to configure or set up the testing environment
  setupFiles: ["./jest.setup.js"],
};
