const SERVICE_CODE = "NJS";

// the object is frozen but is overridden by the secrets
module.exports = Object.freeze({
  SERVICE_CODE,
  SERVICE_NAME: process.env.SERVICE_NAME || "mutation",
  SERVICE_STAGE: process.env.SERVICE_STAGE,
  SERVICE_ROUTE: process.env.SERVICE_ROUTE,
  SERVICE_REGION: process.env.SERVICE_REGION || process.env.AWS_DEFAULT_REGION,
  SERVICE_LOCAL_PORT: Number(process.env.SERVICE_LOCAL_PORT),
  BUS_LOCAL_URI: process.env.BUS_LOCAL_URI,
  VALIDATION_ERROR_CODE: 1,
  VALIDATION_INVALID_STATUS_CODE: Number(
    process.env.SIEVE_INVALID_STATUS_CODE || "422"
  ),
  VALIDATION_RESPONSE_MESSAGE: "INVALID_REQUEST",
  // https://expressjs.com/en/guide/debugging.html
  DEBUG: process.env.DEBUG,
  // https://expressjs.com/en/advanced/best-practice-performance.html#set-node_env-to-production
  NODE_ENV: process.env.NODE_ENV,
  // a shortcut to know if the production environment is on, not expected to be manually set
  IS_PRODUCTION:
    ["production", "prod"].includes(process.env.SERVICE_STAGE) &&
    ["production", "prod"].includes(process.env.NODE_ENV),
});
