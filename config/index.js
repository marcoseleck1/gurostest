const settings = require("./settings");

// this object is going to be shared between all the modules that need to access the configuration values
// so, don't assign to it with `=` because then references will be pointing to the older object
const config = {};

async function loadConfig() {
  Object.assign(config, settings);
  return config;
}

module.exports = { config, loadConfig };
