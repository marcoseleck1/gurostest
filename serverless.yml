service: ${env:SERVICE_NAME}
frameworkVersion: ^2.35.0
variablesResolutionMode: 20210326
configValidationMode: error
useDotenv: true

provider:
  name: aws
  stage: ${opt:stage, 'local'}
  region: ${env:SERVICE_REGION, env:AWS_DEFAULT_REGION}
  runtime: nodejs14.x
  lambdaHashingVersion: 20201221
  tags:
    environment: ${self:provider.stage}
    application: ${env:SERVICE_NAME}
  iam:
    role:
      statements:
        - Effect: Allow
          Action:
            - dynamodb:*
          Resource:
            - { "Fn::GetAtt": ["StatsDynamoDBTable", "Arn"] }

  environment:
    # the following can be overridden by AWS Secrets Manager and are published for each AWS Lambda
    SERVICE_STAGE: ${self:provider.stage}
    SERVICE_NAME: ${env:SERVICE_NAME}
    SERVICE_ROUTE: ${env:SERVICE_ROUTE}
    DEBUG: ${env:DEBUG}

package:
  patterns:
    - "!./**"
    - "config/**"
    - "src/**"
    - "utils/**"
    - "node_modules/**"

functions:
  api:
    handler: src/api.handler
    events:
      - http:
          path: /${env:SERVICE_ROUTE}/validate
          method: post
      - http:
          path: /${env:SERVICE_ROUTE}/stats
          method: get
custom:
  dynamodb:
    stages:
      - local
    start:
      migrate: true
      seed: true
  prune:
    automatic: true
    number: 3

resources:
  Resources:
    StatsDynamoDBTable:
      Type: "AWS::DynamoDB::Table"
      DeletionPolicy: Retain
      Properties:
        AttributeDefinitions:
          - AttributeName: created_at
            AttributeType: S
        KeySchema:
          - AttributeName: created_at
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
        TableName: "${self:service}_${self:provider.stage}_stats"

plugins:
  - serverless-prune-plugin
  - serverless-dynamodb-local
  - serverless-offline
