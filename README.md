# NodeJS

Template for NodeJS microservices using the serverless framework.

## Local Installation

```bash
# install serverless framework globally
$ npm install -g serverless

# install npm dependencies of the project
$ npm ci

# install hooks before committing (requires development dependencies)
$ npm run prepare
# install dynamodb-local
$ serverless dynamodb install



```

## Local Development

```bash
# start the framework offline
sls offline start

# run the linter
npm run lint

# run the style checks
npm run style:check

# apply style
npm run style:write

# test
npm run test

```

## Prod Development

```bash
# deploy
serverless deploy --profile {AWSPrpfile} --stage {ENV}

#endpoints production
https://qdkkkhh0y4.execute-api.us-east-1.amazonaws.com/prod/mutation/validate
https://qdkkkhh0y4.execute-api.us-east-1.amazonaws.com/prod/mutation/stats

```

## Service Environment Configurations

A list of all available configurable options and defaults can be found in `config/settings.js`.

Involved files that configure the service are `config/settings.js` and `serverless.yml` in addition to the ones mentioned below.

- `SERVICE_STAGE=prod`

  - Variables might have been in GitLab project settings. The project inherits group variables.
    - For the `.gitlab-ci.yml` test stage the custom variables set there are used instead.

- `SERVICE_STAGE=dev`

  - Variables might have been in GitLab project settings. The project inherits group variables.
    - For the `.gitlab-ci.yml` test stage the custom variables set there are used instead.

- `SERVICE_STAGE=local`
  - Variables are set in `.env.local`. See the `.env.local.example`.
  - `jest` testing also uses the file `.env.local`.
