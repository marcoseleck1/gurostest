const LIMIT = 4;
const VERTICAL = [];
const DIAGONALR = [];
const DIAGONALL = [];

function compare(current, evaluate) {
  if (current.charAt(0) !== evaluate) {
    return evaluate;
  } else {
    return current + evaluate;
  }
}

function search(sequence, y) {
  let horizontal = "";
  let mutation = false;
  let left = "";
  let right = "";
  let aux = "";
  for (let x = 0; x < sequence.length; x++) {
    // full first
    if (y === 0) {
      DIAGONALR[x] = sequence[x];
      DIAGONALL[x] = sequence[x];
      VERTICAL[x] = sequence[x];
    } else {
      //left -> right
      if (x - 1 >= 0) {
        aux = DIAGONALL[x];
        DIAGONALL[x] = compare(left, sequence[x]);
        left = aux;
      } else {
        left = DIAGONALL[x];
        DIAGONALL[x] = sequence[x];
      }

      //right -> left
      if (x + 1 < sequence.length) {
        right = DIAGONALR[x + 1];
        DIAGONALR[x] = compare(right, sequence[x]);
      } else {
        DIAGONALR[x] = sequence[x];
      }
      // vertical
      VERTICAL[x] = compare(VERTICAL[x], sequence[x]);
    }
    //horizontal
    horizontal = compare(horizontal, sequence[x]);

    // if a mutation is found in any possible way the iteration is stopped
    if (
      horizontal.length === LIMIT ||
      DIAGONALL[x].length === LIMIT ||
      DIAGONALR[x].length === LIMIT ||
      VERTICAL[x].length === LIMIT
    ) {
      mutation = true;
      break;
    }
  }
  return mutation;
}

function validateDNA(dna) {
  let y = 0;
  let mutation = false;
  for (const sequence of dna) {
    if (search(sequence, y)) {
      mutation = true;
      break;
    }
    y++;
  }
  return mutation;
}

module.exports = validateDNA;
