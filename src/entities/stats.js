const AWS = require("aws-sdk");

const params = {
  TableName:
    process.env.SERVICE_NAME + "_" + process.env.SERVICE_STAGE + "_stats",
};

if (process.env.SERVICE_STAGE === "local") {
  AWS.config.update({
    region: process.env.SERVICE_REGION,
    endpoint: "http://localhost:8000",
  });
}
const client = new AWS.DynamoDB.DocumentClient();

async function getStats() {
  let getId = params;
  getId["Key"] = {
    created_at: new Date().toLocaleDateString(),
  };
  let result = await client.get(getId).promise();
  if (result.Item) {
    return result.Item;
  }
  return {};
}

async function updateStats(validation) {
  let item = await getStats();
  let newStats = params;
  newStats["Item"] = getNewStats(item, validation);
  await client.put(newStats).promise();
}

function getNewStats(item, validation) {
  let count_mutations = 0;
  let count_no_mutation = 0;
  let ratio;

  if (Object.keys(item).length > 0) {
    count_mutations = parseInt(item.count_mutations);
    count_no_mutation = parseInt(item.count_no_mutation);
  }

  if (validation) {
    count_mutations++;
  } else {
    count_no_mutation++;
  }
  //CALCULATE RATIO
  ratio = count_mutations / count_no_mutation;

  if (!isFinite(ratio)) {
    ratio = 0;
  }
  return {
    created_at: new Date().toLocaleDateString(),
    count_mutations: count_mutations,
    count_no_mutation: count_no_mutation,
    ratio: ratio,
  };
}

module.exports = { updateStats, getStats };
