const { ValidationError } = require("../../utils/errors");

// configurable middleware pattern described in:
// https://expressjs.com/en/guide/writing-middleware.html
function validateRequest(validator) {
  return (request, response, next) => {
    const result = validator.validate({
      query: request.query,
      params: request.params,
      body: request.body,
    });

    if (result?.error?.details) {
      const details = result.error.details.map((detail) => ({
        attribute: detail.context.label,
        message: detail.message,
      }));
      const error = new ValidationError(details);
      return next(error);
    }

    return next();
  };
}

module.exports = validateRequest;
