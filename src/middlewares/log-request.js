const logger = require("../../utils/logger");

function logRequest(request, response, next) {
  logger.debug(request, "Logging request");
  return next();
}

module.exports = logRequest;
