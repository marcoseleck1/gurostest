const logger = require("../../utils/logger");

function logResponse(request, response, next) {
  logger.debug(response, "Logging response");
  return next();
}

module.exports = logResponse;
