const { config } = require("../../config");
const logger = require("../../utils/logger");
const { Errors, ValidationError } = require("../../utils/errors");

function handleError(error, request, response, _) {
  logger.error(error, "Generic error handler captured unhandled error");

  // istanbul ignore next
  if (response.headersSent) {
    response.end();
  } else if (error instanceof ValidationError) {
    response
      .status(config.VALIDATION_INVALID_STATUS_CODE)
      .json(error.serializable());
  } else {
    response.status(500).json(Errors.build(Errors.UNKNOWN));
  }

  // if middlewares throw error before reaching response logging middleware the response will be lost, so log here too
  logger.debug(response, "Logging response");
}

module.exports = handleError;
