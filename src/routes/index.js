const router = require("express").Router();
const MutationRouter = require("./mutation");

router.use(MutationRouter);

module.exports = router;
