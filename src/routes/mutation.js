const router = require("express").Router();

const CreateMutationRequest = require("../validators/mutation-request");
const validateRequest = require("../middlewares/validate-request");
const { updateStats, getStats } = require("../entities/stats");
const validateDNA = require("../entities/mutation");

async function hasMutation(request, response, next) {
  let result = validateDNA(request.body["adn"]);
  await updateStats(result);
  response.status(200).json({ message: result });
  return next();
}

async function stats(request, response, next) {
  response.status(200).json(await getStats());
  return next();
}

router.post(
  "/mutation/validate",
  validateRequest(CreateMutationRequest),
  hasMutation
);

router.get("/mutation/stats", stats);

module.exports = router;
