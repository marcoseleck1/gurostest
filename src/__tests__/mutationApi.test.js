const { Errors } = require("../../utils/errors");
const request = require("supertest");
const { loadConfig } = require("../../config");
const { createApp } = require("../app");

let app;
let config;

beforeAll(async () => {
  config = await loadConfig();
  app = createApp(config);
});

describe("Mutation API", () => {
  it("DNA validate bad request", async () => {
    const payload = "GSGTS";
    const expected = Errors.build(Errors.VALIDATION);
    await request(app)
      .post("/mutation/validate")
      .send(payload)
      .expect(422)
      .expect((response) => {
        expect(response.body.code).toBe(expected.code);
      });
  });

  it("DNA validate return true", async () => {
    const payload = {
      adn: ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CTCCTA", "TCACTG"],
    };
    await request(app)
      .post("/mutation/validate")
      .send(payload)
      .expect(200)
      .expect((response) => {
        expect(response.body.message).toBe(true);
      });
  });

  it("DNA validate return false", async () => {
    const payload = {
      adn: ["ATGCGA", "CAGTGC", "TTATGT", "AGATTG"],
    };
    await request(app)
      .post("/mutation/validate")
      .send(payload)
      .expect(200)
      .expect((response) => {
        expect(response.body.message).toBe(false);
      });
  });
});
