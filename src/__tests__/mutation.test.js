const validateDNA = require("../entities/mutation");

describe("Mutation function", () => {
  it("DNA validate with mutation inverted diagonal", async () => {
    let adn = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CTCCTA", "TCACTG"];
    let result = validateDNA(adn);
    expect(result).toBeTruthy();
  });

  it("DNA validate with mutation not exists", async () => {
    let adn = ["ATGCGA", "CAGTGC", "TTATGT", "AGATTG"];
    let result = validateDNA(adn);
    expect(result).toBeFalsy();
  });

  it("DNA validate with mutation diagonal", async () => {
    let adn = ["ATGCGA", "CAGTAC", "TTAAGT", "AGATGG", "CTCCTA", "TCACTG"];

    let result = validateDNA(adn);
    expect(result).toBeTruthy();
  });

  it("DNA validate with mutation vertical", async () => {
    let adn = ["TAATTT", "TAGAAC", "TTAAGT", "TAAGGG"];
    let result = validateDNA(adn);
    expect(result).toBeTruthy();
  });

  it("DNA validate with mutation horizontal", async () => {
    let adn = ["TATTTT", "AAGAAC", "TTAAGT", "TAAGGG"];
    let result = validateDNA(adn);
    expect(result).toBeTruthy();
  });
});
