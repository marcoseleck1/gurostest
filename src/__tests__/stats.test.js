const request = require("supertest");

const { createApp } = require("../app");
const { loadConfig } = require("../../config");
const AWS = require("aws-sdk");
const client = new AWS.DynamoDB.DocumentClient();
const tableName =
  process.env.SERVICE_NAME + "_" + process.env.SERVICE_STAGE + "_stats";
let app;
let config;

beforeAll(async () => {
  config = await loadConfig();
  app = createApp(config);
});

describe("stats API", () => {
  beforeAll(async () => {
    let today = new Date().toLocaleDateString();
    await client
      .delete({ TableName: tableName, Key: { created_at: today } })
      .promise();
  });
  it("get stats empty", async () => {
    await request(app)
      .get("/mutation/stats")
      .expect(200)
      .expect((response) => {
        expect(response.body).toEqual({});
      });
  });
  it("DNA validate with mutation inverted diagonal", async () => {
    const payload = {
      adn: ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CTCCTA", "TCACTG"],
    };
    await request(app)
      .post("/mutation/validate")
      .send(payload)
      .expect(200)
      .expect((response) => {
        expect(response.body.message).toBe(true);
      });
  });

  it("get stats already", async () => {
    await request(app)
      .get("/mutation/stats")
      .expect(200)
      .expect((response) => {
        expect(response.body).toHaveProperty("created_at");
        expect(response.body).toHaveProperty("count_mutations");
        expect(response.body).toHaveProperty("count_no_mutation");
        expect(response.body).toHaveProperty("ratio");
      });
  });
});
