// istanbul ignore file
// this module provides the API handler used by the serverless framework

const serverless = require("serverless-http");

const { createApp } = require("./app");
const { loadConfig } = require("../config");
const logger = require("../utils/logger");

let app;
let config;
let handler;

module.exports.handler = async (event, context) => {
  if (!config) {
    config = await loadConfig();
    logger.level = config.DEBUG
      ? "debug"
      : config.IS_PRODUCTION
      ? "info"
      : "debug";
  }
  if (!app) {
    app = createApp(config);
  }
  if (!handler) {
    handler = serverless(app);
  }
  return handler(event, context);
};
