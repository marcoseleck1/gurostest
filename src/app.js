const express = require("express");
const handleError = require("./middlewares/handle-error");
const logRequest = require("./middlewares/log-request");
const logResponse = require("./middlewares/log-response");
const router = require("./routes");

function createApp(config) {
  const app = express();

  app.locals.config = config;
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  // istanbul ignore next
  if (!app.locals.config.IS_PRODUCTION) {
    app.use(logRequest);
  }

  app.use(router);

  // istanbul ignore next
  if (!app.locals.config.IS_PRODUCTION) {
    app.use(logResponse);
  }

  app.use(handleError);

  return app;
}

module.exports.createApp = createApp;
