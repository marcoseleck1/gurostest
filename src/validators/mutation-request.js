const Joi = require("joi");

const CreateMutationRequest = Joi.object({
  body: Joi.object().keys({
    adn: Joi.array().required(),
  }),
  query: Joi.object().keys({}),
  params: Joi.object().keys({}),
});

module.exports = CreateMutationRequest;
